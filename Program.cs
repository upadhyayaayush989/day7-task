﻿using Day_7Task.Model;

List<Product> products;

products = new List<Product>()
{
    new Product(){ProductId=1,ProductName="Asus Mobile"},
    new Product(){ProductId=2,ProductName="Lenovo mobile"},
    new Product(){ProductId=3,ProductName="Iqoo mobile"}
};


List<Shopping> shopping;

shopping = new List<Shopping>()
{
    new Shopping(){ShoppingId=3,ProductId=1,Quantity=10},
    new Shopping(){ShoppingId=4,ProductId=2,Quantity=20},
    new Shopping(){ShoppingId=5,ProductId=4,Quantity=30},
};

var result = from s in shopping
             join p in products
             on s.ProductId equals p.ProductId
             select new
             {
                 ProductId = p.ProductId,
                 matchProductId = s.ShoppingId,
                 matchName = p.ProductName,
                 proQuantity = s.Quantity
             };

foreach(var item in result)
{
    //Console.WriteLine(item.ProductId+ "\t"+ item.matchProductId);
    Console.WriteLine(item);
}